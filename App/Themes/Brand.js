// Brand Colors can be used to quickly adjust Look & Feel of the app.
// None of the Colors is used directly, they are just referenced in appColors and can be fully overwritten there.

const brand = {
  colors: {
    primary: '#005ea8',
    primaryText: '#fff',
    buttonBackground: '#2c3e50',
    buttonText: '#fff',
    backgroundMain: '#fff',
    textMain: '#3c3c3c',
    background1: '#f7f7f7',
    text1: '#2c3e50',
    background2: '#005ea8',
    text2: '#fff',
    grey1: '#525252',
    grey2: '#9B9B9B',
    grey3: '#F8F8F8',
    tick1: '#009900'
  },
  images: {
    logo: require('../Images/app_logo_usz.png'),
    poweredBy: require('../Images/powered_by.png'),
    chatBackground: require('../Images/Chat/bg.png')
  }
}

export default brand
